
# -*- coding: utf-8 -*-

import ngramchv5
import dataPrepPlotly2ch
from difflib import SequenceMatcher
import xlrd
import io
import pandas
import shutil

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

tabsList = ['freyapersf','freyaperds','freyallsf','freyallds','adolfper','adolfll','freyacondsf','freyacondds','freyafoam','adolffoam','abbfoam']
for each in range(11):
    sheetName = tabsList[each]

    column = read_column_in_excel_with_startRow('fen.xls',sheetName,0,0)
    thefile = io.open(sheetName+'.txt', 'w',encoding='utf-8')

    for item in column:
        thefile.write("%s\n" % item)

    ngramchv5.job('freyapersf.txt','11111')
    dataPrepPlotly2ch.job('11111grams.txt',sheetName)