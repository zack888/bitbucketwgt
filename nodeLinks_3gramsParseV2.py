#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 29 22:53:58 2018

@author: tzehau
"""

import pandas as pd
from collections import defaultdict

#### import with xls or xlsx - result file from WGT
xl=pd.ExcelFile('./carFrebreeze_ngrams_results.xlsx')
# check sheet_names


df_Unigram = xl.parse('Unigram')
df_Unigram = df_Unigram[df_Unigram['POS'].isin(['Ag', 'a', 'an', 'ad', 'Ng', 'n', 'nr', 'ns', 'nt', 'nz', 's', 't', 'z', 'BG', 'b', 'zg'])]
#df_Unigram = df_Unigram[df_Unigram['POS'].isin(['Ng', 'n', 'nr', 'ns', 'nt', 'nz', 's'])]
df_Unigram = df_Unigram.sort_values('Count', ascending=False)
selected_Unigram = list(df_Unigram.index[0:40])
selected_Unigram.remove('京东')


#print(xl.sheet_names)
df = xl.parse('3grams')
df=df[df['n3grams'].str.match('|'.join(selected_Unigram))]
df=df.loc[df['Count'] > 1]
df.index = range(len(df))
df=df.iloc[:,[0,1,2]]

#df = pd.read_csv('test.txt', sep="\t")
T1 = pd.Series([])
T2 = pd.Series([])
T2Dup = pd.Series([])
for i in range(0,len(df)):
    xx=df.iloc[i,0].split("|")
    T1=T1.set_value(i, xx[0])
    T2=T2.set_value(i, xx[1] + xx[2])
    T2Dup=T2Dup.set_value(i+len(df), xx[1] + xx[2])
    
dfrefLinks=pd.concat([df, T1, T2], axis=1)
dfrefLinks.columns=["ngram", "Count", "Mean_Rating", "T1", "T2"]

tmp1=pd.concat([df, T1], axis=1)
df=df.set_index(T2Dup.index.values)
tmp2=pd.concat([df, T2Dup], axis=1)
dfrefNodes=pd.concat([tmp1, tmp2], axis=0)
dfrefNodes.columns=["ngram", "Count", "Mean_Rating", "Term"]

### create nodes dataframe & dictionary for index
UniqTerm = dfrefNodes['Term'].unique()
wordID = defaultdict(int)
nodeName = pd.DataFrame(dtype='object')
nodeID = pd.DataFrame(dtype='object')
nodeGroup = pd.DataFrame(dtype='object')
nodeRating = pd.DataFrame(dtype='object')
for i in range(0, len(UniqTerm)):        
    
    wordID[UniqTerm[i]]=i
    
    Group=""
    yy=dfrefNodes[dfrefNodes['Term'].str.match(UniqTerm[i])]
    rating = (yy['Count']*yy['Mean_Rating']).sum()/yy['Count'].sum()
    
    if rating<=1: Group = '0-1'
    if rating> 1 and rating<=2: Group = '1-2'
    if rating> 2 and rating<=3: Group = '2-3'
    if rating> 3 and rating<=4: Group = '3-4'
    if rating> 4: Group = '4-5'
    
    nodeName =  nodeName.append([UniqTerm[i]])
    nodeID = nodeID.append([i])
    nodeGroup = nodeGroup.append([Group])
    nodeRating = nodeRating.append([rating])

dfNodes = pd.concat([nodeName, nodeID, nodeGroup, nodeRating], axis=1)  
dfNodes.columns=["Term", "Index", "Group", "Rating"]

linkSource = pd.DataFrame(dtype='object')
linkTarget = pd.DataFrame(dtype='object')
linkFreq = pd.DataFrame(dtype='object')
### create links dataframe
for i in range(0, len(dfrefLinks)):
    linkSource =  linkSource.append([wordID[dfrefLinks['T1'][i]]])
    linkTarget =  linkTarget.append([wordID[dfrefLinks['T2'][i]]])
    linkFreq = linkFreq.append([dfrefLinks['Count'][i]])
dflinks = pd.concat([linkSource, linkTarget, linkFreq], axis=1)
dflinks.columns=["source", "target", "Freq"]

dfNodes.to_csv('./3gramNodes.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")
dflinks.to_csv('./3gramLinks.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")



    