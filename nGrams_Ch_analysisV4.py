#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 15:30:50 2018

@author: tzehau
"""
import jieba
import jieba.posseg as pseg
import jieba.analyse
from nltk import ngrams
import numpy
import pandas as pd
import re
from collections import Counter
from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from googletrans import Translator


#### import with xls or xlsx - result file from WGT
xl=pd.ExcelFile('./DataSets/Data_carFrebreeze.xls')
# check sheet_names
#print(xl.sheet_names)
df = xl.parse('yourData')
##df=df.drop_duplicates(subset='Review', keep="first")

##### CHANGE "Review" TO THE COLUMN NAME IN YOUR EXCEL 
dupBoolean = df.Review.duplicated(keep='first')
dupdf=df[dupBoolean]
df=df[~dupBoolean]
dupKeep = [x for x in dupdf.iloc[:,0] if len(x)<=12]

#### import data from tab delimited file
#df = pd.read_csv('./DataSets/HotelNeg.txt', sep="\t", header=None)

## load stopwords file
stopwords = [ line.strip('\n') for line in open('./chinese_stop_wordsV2.txt',"r") ]

def segSentence(paragraph):
    for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
        yield sent
        
def countAdaptedPos(grams, posTag):
    gramsPos=[]
    for x,y in grams:
        gramsPos.append(y)
    
    gramsPos = Counter(gramsPos)
    count = 0
    for x,y in gramsPos.items():
        if x in posTag:
            count=count + y
    return count

def jiebaTokenizer(text):
    text=re.sub("\W+", "", text) # remove punctuation
    return list(jieba.cut(text, cut_all=False))

def spamCheck(df):
    tfVectorizer = CountVectorizer(tokenizer=jiebaTokenizer)
    tfVector=tfVectorizer.fit_transform(df.iloc[:,0])
    similarity_matrix = cosine_similarity(tfVector)

    zz=[]
    for i in range(0, len(df)):
        xx=similarity_matrix[i,:]
        xx[i]=0
        #if numpy.any(xx>0.95):
        if numpy.all(xx<0.95):
            zz.append(df.iloc[i,0])
    return zz

n2grams_df = pd.DataFrame(dtype='object')
n2grams_Rating = pd.DataFrame(dtype='object')

n3grams_df = pd.DataFrame(dtype='object')
n3grams_Rating = pd.DataFrame(dtype='object')

n4grams_df = pd.DataFrame(dtype='object')
n4grams_Rating = pd.DataFrame(dtype='object')

jieba.enable_parallel(4)
spamRemoved=spamCheck(df)
spamRemoved = spamRemoved + dupKeep
posTag=['Ag', 'a', 'an', 'ad', 'Ng', 'n', 'nr', 'ns', 'nt', 'nz', 's', 't', 'z', 'Vg', 'v', 'vd', 'vn', 'BG', 'b', 'zg']
wordCount = defaultdict(int)
wordRating = defaultdict(float)
wordPos = defaultdict()
for i in range(0, len(spamRemoved)):
        
    if len(spamRemoved[i].split("star"))==2:
        text, rating  = spamRemoved[i].split("star")
        rating = float(rating)
    else:
        continue ## skip review without star rating
       
    xx=list(segSentence(text))    
    for sen in xx:
        pos = list(pseg.cut(sen))
        pos=[tuple(v) for v in pos]
        reStopWords_pos = [v for v in pos if v[0] not in stopwords]
        
        ### word count, rating & POS
        for word, pos in reStopWords_pos:
            wordCount[word] += 1
            wordRating[word] += rating
            wordPos[word] = pos
        
        ### 2grams
        twograms = list(ngrams(reStopWords_pos, 2))
        for grams in twograms:            
            count=countAdaptedPos(grams, posTag)             
            if count>=2:
                n2grams_df =  n2grams_df.append([grams[0][0] + '|' + grams[1][0]])
                n2grams_Rating = n2grams_Rating.append([rating])
        
        ### 3grams
        threegrams = list(ngrams(reStopWords_pos, 3))
        for grams in threegrams:            
            count=countAdaptedPos(grams, posTag)
            if count>=2:               
                n3grams_df =  n3grams_df.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0]])
                n3grams_Rating = n3grams_Rating.append([rating])
                
        ### 4grams
        fourgrams = list(ngrams(reStopWords_pos, 4))
        for grams in fourgrams:
            count=countAdaptedPos(grams, posTag)                
            if count>=2: 
                n4grams_df =  n4grams_df.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0] + '|' + grams[3][0]])
                n4grams_Rating = n4grams_Rating.append([rating])
                
wordCount=pd.DataFrame([wordCount]).transpose()
wordRating=pd.DataFrame([wordRating]).transpose()
wordPos=pd.DataFrame([wordPos]).transpose()
FOM = pd.concat([wordCount, wordRating, wordPos], axis=1)
FOM.columns=['Count', 'Rating', 'POS']
FOM['Rating']=FOM['Rating']/FOM['Count']

translator = Translator() # note include translator will be slow; max 1500 characters for each call
n2grams_results=pd.concat([n2grams_df, n2grams_Rating], axis=1)
n2grams_results.columns=['n2grams', 'Rating']
n2grams_wordFreq = n2grams_results.groupby('n2grams').count()
n2grams_MeanRating = n2grams_results.groupby('n2grams').mean()
# translation
n2grams_en = pd.DataFrame(dtype='object') # for translation
for i in range(0, len(n2grams_wordFreq.index)):
    xx=translator.translate(n2grams_wordFreq.index[i]).text
    n2grams_en = n2grams_en.append([xx])
n2grams_en.index=n2grams_wordFreq.index
n2grams_results=pd.concat([n2grams_wordFreq, n2grams_MeanRating, n2grams_en], axis=1)
n2grams_results.columns=['Count', 'Mean_Rating', 'En_Translation']

translator = Translator() # note include translator will be slow; max 1500 characters for each call
n3grams_results=pd.concat([n3grams_df, n3grams_Rating], axis=1)
n3grams_results.columns=['n3grams', 'Rating']
n3grams_wordFreq = n3grams_results.groupby('n3grams').count()
n3grams_MeanRating = n3grams_results.groupby('n3grams').mean()
# translation
n3grams_en = pd.DataFrame(dtype='object') # for translation
for i in range(0, len(n3grams_wordFreq.index)):
    xx=translator.translate(n3grams_wordFreq.index[i]).text
    n3grams_en = n3grams_en.append([xx])
n3grams_en.index=n3grams_wordFreq.index
n3grams_results=pd.concat([n3grams_wordFreq, n3grams_MeanRating, n3grams_en], axis=1)
n3grams_results.columns=['Count', 'Mean_Rating', 'En_Translation']

translator = Translator() # note include translator will be slow; max 1500 characters for each call
n4grams_results=pd.concat([n4grams_df, n4grams_Rating], axis=1)
n4grams_results.columns=['n4grams', 'Rating']
n4grams_wordFreq = n4grams_results.groupby('n4grams').count()
n4grams_MeanRating = n4grams_results.groupby('n4grams').mean()
# translation
n4grams_en = pd.DataFrame(dtype='object') # for translation
for i in range(0, len(n4grams_wordFreq.index)):
    xx=translator.translate(n4grams_wordFreq.index[i]).text
    n4grams_en = n4grams_en.append([xx])
n4grams_en.index=n4grams_wordFreq.index    
n4grams_results=pd.concat([n4grams_wordFreq, n4grams_MeanRating, n4grams_en], axis=1)
n4grams_results.columns=['Count', 'Mean_Rating', 'En_Translation']

#### write results to excel
writer = pd.ExcelWriter('carFebreeze_ngrams_results.xlsx', engine='xlsxwriter')
FOM.to_excel(writer, 'Unigram', header=True, index=True)
n2grams_results.to_excel(writer, '2grams', header=True, index=True)
n3grams_results.to_excel(writer, '3grams', header=True, index=True)
n4grams_results.to_excel(writer, '4grams', header=True, index=True)
writer.save()

